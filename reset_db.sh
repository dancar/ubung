#!/bin/bash

meteor shell <<EOF
import { reset_db } from  './server/reset_db.js'
reset_db();
console.log('done.');
EOF
