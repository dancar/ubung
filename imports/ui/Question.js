import React from 'react'
import Article from './Questions/Article'
import Choice from './Questions/Choice'

export default class Question extends React.Component {
    render () {
        if (this.props.question.type === "article") {
            return (
                <Article
                  question={this.props.question}
                  onAnswer={this.props.onAnswer}
                  showAsAnswer={this.props.showAsAnswer}
                  ></Article>
            )
        }

        if (this.props.question.type === "choice") {
            return (
                <Choice
                  question={this.props.question}
                  onAnswer={this.props.onAnswer}
                  showAsAnswer={this.props.showAsAnswer}
                  >
                </Choice>
            )
        }
        return "Invalid question type: " + this.props.question.type
    }
}
