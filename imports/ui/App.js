import React, { Component } from 'react'
import { Meteor } from 'meteor/meteor'

import { withTracker } from 'meteor/react-meteor-data'

import AccountsUIWrapper from './AccountsUIWrapper'
import { Questions } from '../api/questions.js'
import  Question  from './Question.js'

const UNANSWERED = 0
const CORRECT = 1
const WRONG = 2

const STATUS_BORDER_COLOR = {
    [UNANSWERED]: "gray",
    [CORRECT]: "green",
    [WRONG]: "red"
}

const SUCCESS_SYMBOL = {
    [CORRECT]: " ✅ ",
    [WRONG]: "❌"
}

class App extends Component {

    constructor (props) {
        super(props);
        this.state = {
            lastAnswerStatus: UNANSWERED,
            currentQuestion: null
        }
    }

    pickQuestion () {
        const lastQuestion = this.state.currentQuestion
        const newQuestionIndex = Math.floor(Math.random() * this.props.questions.length)
        this.setState({
            lastQuestion,
            currentQuestion: this.props.questions[newQuestionIndex]
        })
    }

    handleAnswer (correct) {
        console.log(this.state.currentQuestion, correct);
        this.setState({
            lastAnswerStatus: correct ? CORRECT : WRONG
        })
        this.pickQuestion()
    }

    componentWillReceiveProps (newProps) {
        if (!this.props.questions.length && newProps.questions.length) {
            setTimeout(() => this.pickQuestion(), 1)
        }
    }

    render () {


        if (!this.props.currentUser) {
            return (
              <AccountsUIWrapper/>
            )
        }

        if (!this.state.currentQuestion) {
            return "Loading questions..."
        }

        return (
            <div>
              <div className="question"
                   style={{borderColor: STATUS_BORDER_COLOR[this.state.lastAnswerStatus]}}>
                <Question
                  question={this.state.currentQuestion}
                  onAnswer={this.handleAnswer.bind(this)} ></Question>
              </div>
              { this.state.lastQuestion && (
                  <div className="last-answer">
                    <div>
                      { SUCCESS_SYMBOL[this.state.lastAnswerStatus] }
                      &nbsp;
                    </div>
                    <Question showAsAnswer question={this.state.lastQuestion}></Question>
                  </div>
              )}
              <div className="footer">
                <AccountsUIWrapper />
                <div style={{flex: 1, textAlign: "right"}}>
                Total number of available questions: {this.props.questions.length}
                </div>
              </div>
            </div>
        )
    }
}

export default withTracker(() => {
    return {
        currentUser: Meteor.user(),
        questions: Questions.find({}).fetch()
    };
})(App);
