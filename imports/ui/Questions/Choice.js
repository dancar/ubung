import React from 'react'
import { Button } from 'react-bootstrap';

export default class Choice extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            selectedChoice: null,
            choices: this.setChoices(this.props.question)
        }
    }

    setChoices (question) {
        return this.shuffleArray([question.answer].concat(question.wrongChoices))
    }

    componentWillReceiveProps (newProps) {
        if (this.props.question != newProps.question ) {
            this.setState({
                selectedChoice: null,
                choices: this.setChoices(newProps.question)
            })
        }
    }

    // Credit: https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    shuffleArray (array) {
        for (let i = array.length - 1; i > 0; i--) {
            let j = Math.floor(Math.random() * (i + 1));
            [array[i], array[j]] = [array[j], array[i]];
        }
        return array
    }

    answerClicked () {
        const correct = this.state.selectedChoice === this.props.question.answer
        this.props.onAnswer(correct)
    }

    renderAsAnswer () {
        return `${this.props.question.text}: ${this.props.question.answer}`
    }

    renderChoices () {
        const choices = this.state.choices.map( (opt, index) => {
            const id = `choice_${index}`
            const checked = this.state.selectedChoice ===  opt
            return (
                <div key={id} className={`choice-option ${checked ? "choice-checked" : ""}`}>
                  <input
                    type="radio"
                    name="choice"
                    id={id}
                    onClick={() => this.setState({selectedChoice: opt})}
                    checked={checked}
                    />
                  <label htmlFor={id}>
                    { opt }
                  </label>
                </div>
            )})
        return (
            <div className="choices-container">
              {choices}
            </div>
        )
    }

    render () {
        if (this.props.showAsAnswer) {
            return this.renderAsAnswer()
        }

        return (
            <div>
              <div className="question-choice">
                "{this.props.question.text}"
              </div>
              { this.renderChoices() }
              <Button
                disabled={!this.state.selectedChoice}
                onClick={this.answerClicked.bind(this)}>
                OK
              </Button>
            </div>
        )
    }
}
