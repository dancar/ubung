import React from 'react'
import { Button } from 'react-bootstrap';

const styleForArticle = {
    "die" : {color: "red"},
    "das" : {color: "green"},
    "der" : {color: "blue"}
}

export default class Article extends React.Component {
    answerClicked (e) {
        const answer = e.target.value
        const correct = answer == this.props.question.article
        this.props.onAnswer(correct)
    }

    renderAsAnswer () {
        return (
            <span>
              { this.props.question.article }
              &nbsp;
              <span style={styleForArticle[this.props.question.article]}>
                { this.props.question.noun }
              </span>
              <span className="question-translation">
                ({this.props.question.translation})
              </span>
            </span>
        )
    }

    render () {
        if (this.props.showAsAnswer) {
            return this.renderAsAnswer()
        }

        const noun = this.props.question
        return (
            <div>
              <div className="question-noun">
                "{noun.noun}"
              </div>
              <Button value="die" style={styleForArticle.die} className="red-text" onClick={this.answerClicked.bind(this)}>die</Button>
              <Button value="das" style={styleForArticle.das} onClick={this.answerClicked.bind(this)}>das</Button>
              <Button value="der" style={styleForArticle.der} onClick={this.answerClicked.bind(this)}>der</Button>
            </div>
        )
    }
}
