import { Questions } from '../imports/api/questions.js'

const ARTICLES = ["das", "der", "die"]

export function reset_db () {
    console.log("Reseting DB...")
    Questions.remove({})

    const nouns = JSON.parse(AppAssets.getText("nouns.json"))
    for (let noun of Object.keys(nouns)) {
        let [translation, article] = nouns[noun]
        Questions.insert({
            type: "article",
            noun, article, translation
        })
    }

    const choices = JSON.parse(AppAssets.getText("choices.json"))
    for (let choice of choices ) {
        console.log(choice)
        Questions.insert({
            type: "choice",
            ...choice
        })
    }

    const prepositions = JSON.parse(AppAssets.getText("prepositions.json"))
    for (let typ of Object.keys(prepositions)) {
        let wrongChoices = Object.keys(prepositions).filter( otherTyp => otherTyp !== typ )
        for (let preposition of prepositions[typ]) {
            Questions.insert({
                type: "choice",
                text: `The Preposition "${preposition}" implies`,
                answer: typ,
                wrongChoices: wrongChoices
            })
        }
    }
    console.log("DB Reset done.")

}
